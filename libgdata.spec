Name:           libgdata
Version:        0.18.1
Release:        1
Summary:        GLib-based library for accessing online service APIs using the GData protocol
License:        LGPLv2+
URL:            https://live.gnome.org/%{name}
Source0:        https://download.gnome.org/sources/%{name}/0.18/%{name}-%{version}.tar.xz
BuildRequires:  gcr-devel glib2-devel gnome-online-accounts-devel gobject-introspection-devel 
BuildRequires:  gtk-doc intltool json-glib-devel liboauth-devel libsoup-devel libxml2-devel 
BuildRequires:  vala vala-devel meson

%description
libgdata is a GLib-based library for accessing online service APIs using the
GData protocol --- most notably, Google's services. It provides APIs to access
the common Google services, and has full asynchronous support.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
%{name}-devel contains the header files for developing
applications that want to make use of %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
%meson \
  -Dalways_build_tests=false \
  -Dinstalled_tests=false \
  -Dgtk_doc=true \
  %{nil}
%meson_build

%install
%meson_install
%delete_la

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_datadir}/locale/*
%{_libdir}/libgdata.so.*
%{_libdir}/girepository-1.0/*

%files devel
%defattr(-,root,root)
%{_includedir}/libgdata/*
%{_libdir}/libgdata.so
%{_libdir}/pkgconfig/libgdata.pc
%{_datadir}/vala/vapi/libgdata.*
%{_datadir}/gir-1.0/*.gir

%files help
%defattr(-,root,root)
%doc NEWS README
%{_datadir}/gtk-doc/html/*

%changelog
* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.18.1-1
- Upgrade to 0.18.1
- Update Version, Release, Source0, stage 'files'
- Stage 'build' use meson build

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.17.9-4
- Package init

